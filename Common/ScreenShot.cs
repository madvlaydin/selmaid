using Allure.Commons;
using NUnit.Framework;
using OpenQA.Selenium;
using Common;
using System;

namespace Utils
{
    public class Screenshots
    {
        /// <summary>
        /// Создает скриншот в формате jpeg в каталоге скриншотов и добавляет его в отчет аллюр
        /// </summary>
        public static void MakeScreenshot()
        {
            Screenshot ss = ((ITakesScreenshot)BaseSteps.Browser).GetScreenshot();

            string testName = TestContext.CurrentContext.Test.Name;

            string screenshotName = DateTime.Now.TimeOfDay.ToString().Replace(":", "-").Substring(0, 8) + " " + testName + ".jpeg";

            ss.SaveAsFile($"{ProjectEnvironment.AllureResults}\\" + screenshotName, ScreenshotImageFormat.Jpeg);

            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, tc =>
            {
                int i = tc.steps.FindIndex(step => step.status != Status.passed && step.status != Status.skipped);
                // Ищем шаг, статус которого не равен успешному и пропущенному. Такой будет 1 и может быть красным либо фиолетовым

                tc.steps[i].attachments.Add(new Attachment
                {
                    name = "Скриншот ошибки",
                    source = screenshotName
                });

            });
        }
    }
}
