using System.Collections.Generic;
using Gurock.TestRail;
using System.Linq;
using NUnit.Framework;
using Newtonsoft.Json.Linq;
using System;
using Common;
using Sources.Common;

namespace Utils
{
    public class TestRailResults
    {
        [ThreadStatic] public static int Pr;
        [ThreadStatic] public static int TS;
        [ThreadStatic] public static int Section;
        [ThreadStatic] public static int Run;

        [ThreadStatic] private static string time = null;
        [ThreadStatic] private static string TestCaseName = null;

        private static APIClient client = new APIClient("https://testrail/")
        {
            User        = URLs.TestRailLogin,
            Password    = URLs.TestRailApiKey
        };

        public static void TestRailResult()
        {
            time = (DateTime.Now.TimeOfDay - BaseSteps.start.TimeOfDay).Minutes.ToString();

            TestCaseName = TestContext.CurrentContext.Test.Properties.Get("Description").ToString();

            JObject TestCase = FindTestCaseInTestRail();

            if (TestCase != null)
                AddResultInTestRail(TestCase);
        }

        /// <summary>
        /// Проверяем наличие тест-кейса TestRail, если его нет, то создаём. 
        /// Возвращает JObject TestCase.
        /// </summary>
        /// <param name="TestCaseName"></param>
        private static JObject FindTestCaseInTestRail()
        {
            try
            {
                JArray allTestsInTestSuite = (JArray)client.SendGet($"get_cases/{Pr}/&suite_id={TS}");

                JObject TestCase = (JObject)allTestsInTestSuite.FirstOrDefault(
                    testCase => testCase["title"].Value<string>() == TestCaseName);

                if(TestCase == null)
                {
                    allTestsInTestSuite = CreateTestCaseInTestRail();

                    TestCase = (JObject)allTestsInTestSuite.FirstOrDefault(
                        testCase => testCase["title"].Value<string>() == TestCaseName);

                    return (JObject)allTestsInTestSuite.First(testCase => testCase["title"].Value<string>() == TestCaseName);
                }
                return TestCase;
            }
            catch
            {
                AllureReport.SetTestRailStatus("Ошибка при обращении в TestRail (HTTP-400)");
                return null;
            }
        }

        //Создание ТекстКейса в ТестРейле
        private static JArray CreateTestCaseInTestRail()
        {
            List<JToken> TestRailSteps = GenerateStepsForTestRail();

            JObject newTestCase = new JObject
            {
                { "title" , TestCaseName },
                { "custom_estimation", time+"m"},
                { "custom_automation_type", 4},
                { "custom_status_auto", 5},
                { "custom_steps_separated", JToken.FromObject(TestRailSteps)}
            };

            JArray GetSections = (JArray)client.SendGet($"get_sections/{Pr}/&suite_id={TS}");

            string sectionName;
            try
            { sectionName = BaseSteps.featureContext.FeatureInfo.Description.Replace("\t", "").Trim(' '); }
            catch(Exception)
            { throw new Exception("Не могу создать секцию в TestRail, потому что у feature-файла не указан Description функции"); }

            foreach(var section in GetSections)
                if(section["name"].ToString().Contains(sectionName))
                    Section = (int)section["id"];

            if(Section == 0)
                CreateSectionsInTestRail();

            client.SendPost($"add_case/{Section}", newTestCase);
            JArray allTestsInTestSuite = (JArray)client.SendGet($"get_cases/{Pr}/&suite_id={TS}");

            return allTestsInTestSuite;
        }

        private static void CreateSectionsInTestRail()
        {
            JArray GetSections = (JArray)client.SendGet($"get_sections/{Pr}/&suite_id={TS}");
            string sectionName = BaseSteps.featureContext.FeatureInfo.Description.Replace("\t", "").Trim(' ');

            JObject newSection = new JObject
            {
                { "name" , sectionName },
                { "suite_id", TS }
                //{ "parent_id", parentSectionId }
            };

            client.SendPost($"add_section/{Pr}", newSection);

            GetSections = (JArray)client.SendGet($"get_sections/{Pr}/&suite_id={TS}");

            foreach(var section in GetSections)
                if(section["name"].ToString().Contains(sectionName))
                    Section = (int)section["id"];
        }

        //Создание шагов для ТекстКейсов
        private static List<JToken> GenerateStepsForTestRail()
        {
            //Создаём описание шагов
            List<JToken> TestRailSteps = new List<JToken>();

            for(int i = 0; i < BaseSteps.scenarioSteps.Count; i++)
            {
                TestRailSteps.Add(new JObject
                {
                    { "content", BaseSteps.scenarioSteps[i].ToString()},
                    { "expected", "Pass"}
                });
            }

            return TestRailSteps;
        }

        /// <summary>
        /// Взять последний TestRun и Добавить результат выполнения теста в TestRail
        /// </summary>
        /// <param name="TestCaseName"></param>
        /// <param name="TestCase"></param>
        private static void AddResultInTestRail(JObject TestCase)
        {
            var testResult = new Dictionary<string, object>
            {
                {  "status_id"  , 1  } ,
                {  "comment"    , "Passed!" }
            };

            if(BaseSteps.scenarioTags.Contains("disable"))
            {
                testResult["status_id"] = 6;
                testResult["comment"]   = "Тест временно отключен";
            }
            else if(BaseSteps.scenarioTags.FindAll(x => x.Contains("PSB")).Count() != 0)
            {
                if(BaseSteps.scenarioContext.TestError != null)
                {
                    testResult["status_id"] = 4;
                    testResult["comment"]   = $"Тест не прошёл. Ошибка заведена в Jira: '{string.Join(", ", BaseSteps.scenarioTags.FindAll(x => x.Contains("PSB")))}' " +
                    $"\n\n"+ErrorHandling.Message(BaseSteps.scenarioContext.TestError.ToString())+
                    "\n\nSpecFlow error: " + BaseSteps.scenarioContext.TestError.ToString();
                }
                else
                {
                    testResult["status_id"] = 2;
                    testResult["comment"]   = $"Тест прошёл. Нужно удалить тег с ссылкой на ошибку в Jira: '{string.Join(", ", BaseSteps.scenarioTags.FindAll(x => x.Contains("PSB")))}'";
                }
            }
            else if(BaseSteps.scenarioContext.TestError != null)
            {
                testResult["status_id"] = 5;
                testResult["comment"]   = ErrorHandling.Message(BaseSteps.scenarioContext.TestError.ToString()) + "\n\nSpecFlow error: " + BaseSteps.scenarioContext.TestError.ToString();
            }

            JArray allRuns = (JArray)client.SendGet($"get_runs/{Pr}");

            foreach(JObject run in allRuns)
                if(run["name"].ToString().Contains("Smoke") && 
                (bool)run["is_completed"] is false && 
                (int)run["id"] > Run)
                    Run = (int)run["id"];

            if (Run == 0)
                AllureReport.SetTestRailStatus("Для автотестов в TestRail не найдено открытых Run");
            else
                client.SendPost($"add_result_for_case/{Run}/" + TestCase["id"], testResult);
        }
    }
}
