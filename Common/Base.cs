using Allure.Commons;
using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.IE;
using Utils;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using TechTalk.SpecFlow;
using System.Threading;
using OpenQA.Selenium.Remote;
using NUnit.Framework;
using System.Net;
using Sources.Common;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Management;
using System.Text.RegularExpressions;

namespace Common
{
    [Binding]
    public class BaseSteps : IDisposable
    {
        [ThreadStatic] public static IWebDriver Browser;
        [ThreadStatic] public static Process driverProcess;

        [ThreadStatic] public static ScenarioContext scenarioContext;
        [ThreadStatic] public static FeatureContext featureContext;
        [ThreadStatic] public static List<string> scenarioTags;
        [ThreadStatic] public static List<string> scenarioSteps;
        [ThreadStatic] public static DateTime start;
        [ThreadStatic] public static TestContext testContext;
        [ThreadStatic] public static AllureLifecycle allure;

        [ThreadStatic] public static string AllureScenarioUUID;
        [ThreadStatic] public static string DBOUIversion = "Версия UI";
        [ThreadStatic] public static string DBOMonolithVersion = "Версия MTH";

        [ThreadStatic] public static string CurrentBankUser;
        [ThreadStatic] public static string BrowserPort;

        public BaseSteps(ScenarioContext _scenarioContext, FeatureContext _featureContext, TestContext _testContext)
        {
            allure = AllureLifecycle.Instance;
            start = DateTime.Now;

            TestRailResults.Pr = 6;
            TestRailResults.TS = 22;
            TestRailResults.Section = 0;
            TestRailResults.Run = 0;

            scenarioContext = _scenarioContext;
            featureContext = _featureContext;
            testContext = _testContext;

            scenarioTags = scenarioContext.ScenarioInfo.Tags.ToList();

            scenarioSteps = new List<string>();

            CurrentBankUser = Scenarios.GetUserToRunBrowser();

            allure.UpdateTestCase(tc => AllureScenarioUUID = tc.uuid);
        }

        public static void Initialize()
        {
            if(ProjectEnvironment.BrowserName == "Chrome")
            {
                var options = new ChromeOptions()
                {
                    AcceptInsecureCertificates = true
                };

                options.SetLoggingPreference(LogType.Driver, LogLevel.All);

                options.AddUserProfilePreference("credentials_enable_service", false);
                options.AddUserProfilePreference("profile.password_manager_enabled", false);
                options.AddUserProfilePreference("download.default_directory", ProjectEnvironment.DownloadAndUploadFiles);

                options.AddArgument("--force-device-scale-factor=0.65");

                options.AddArgument($"--auth-server-whitelist=rbp-app.headoffice.psbank.local,tstdbokb-bank");
                options.AddArgument($"--auth-negotiate-delegate-whitelist=rbp-app.headoffice.psbank.local,tstdbokb-bank");

                options.AddArgument("--window-size=1920,1200");

                options.AddArgument("--enable-logging");

                options.AddArgument("--no-sandbox");

                options.AddArgument("--enable-automation");

                options.AddArgument($"--log-net-log={ProjectEnvironment.TestLogs}\\{AllureScenarioUUID}.json");

                options.AddExtension(ProjectEnvironment.FullRuTokenPathChrome);

                if (CurrentBankUser.Equals("local"))
                {
                    Browser = new ChromeDriver(ProjectEnvironment.ChromeDriverDir, options);
                }
                else
                {
                    using (new ActiveDirectory.User(TestUsers.ADUsers[CurrentBankUser].Login, 
                                                    TestUsers.ADUsers[CurrentBankUser].Domain, 
                                                    TestUsers.ADUsers[CurrentBankUser].Password))
                    {
                        for (int i = 0; i <= 5; i++)
                        {
                            try
                            {
                                Browser = new RemoteWebDriver(new Uri($"http://localhost:{BrowserPort}", UriKind.Absolute), options);

                                break;
                            }
                            catch(Exception e)
                            {
                                if(i==5)
                                    throw new Exception(e.ToString());
                                else
                                    Thread.Sleep(5000);
                            }
                        }

                        driverProcess = ActiveDirectory.GetChromeDriverProcess();
                    }
                }
            }
            else if(ProjectEnvironment.BrowserName == "IE")
            {
                InternetExplorerOptions ieOptions = new InternetExplorerOptions
                {
                    IntroduceInstabilityByIgnoringProtectedModeSettings = true,
                    RequireWindowFocus                                  = false,
                    IgnoreZoomLevel                                     = true,
                    UsePerProcessProxy                                  = true,
                };

                ieOptions.AddAdditionalCapability("enableElementCacheCleanup", "false", true);
                ieOptions.AddAdditionalCapability("javascriptEnabled", "true", true);

                Browser = new InternetExplorerDriver(ProjectEnvironment.IEDriverPath, ieOptions);
            }
            else if(ProjectEnvironment.BrowserName == "Yandex")
            {
                string profilePath = YandexProfile.CreateProfileDir();

                ChromeOptions options = new ChromeOptions
                {
                    AcceptInsecureCertificates = true
                };

                options.AddArgument("--start-maximized");
                options.AddArgument("--disable-infobars");

                options.AddArgument($"--user-data-dir={profilePath}");
                options.AddArgument("--profile-directory=Profile 1");

                options.BinaryLocation = Environment.ExpandEnvironmentVariables("%USERPROFILE%\\AppData\\Local\\Yandex\\YandexBrowser\\Application\\browser.exe");

                Browser = new ChromeDriver(ProjectEnvironment.ChromeDriverPath, options);
            }
            else
                throw new Exception("Драйвер не найден");
        }

        public void Dispose()
        {
            if(!scenarioTags.Contains("disable") && Browser != null)
            {
                AllureReport.GetLogsFromBrowser();

                Browser.Quit();

                if (!CurrentBankUser.Equals("local"))
                    driverProcess.Kill();

                AllureReport.GetRequestIDs();

                AllureReport.AddConsoleLogAndRequestIDs();

                Browser = null;
            }

            if(ProjectEnvironment.BrowserName == "Yandex")
            {
                YandexProfile.DeleteProfileDir();
            }
        }

        [BeforeStep]
        public void BeforeStep()
        {
            string step = null;
            string table = null;

            step = scenarioContext.StepContext.StepInfo.Text.ToString();
            try
            { table = scenarioContext.StepContext.StepInfo.Table.ToString(); }
            catch { }

            scenarioSteps.Add($"{step} \n\r {table}");

            if (!scenarioTags.Contains("disable") && !scenarioTags.Contains("ManualBrowserManage") && Browser == null)
            {
                Directory.CreateDirectory(ProjectEnvironment.TestLogs);

                Initialize();
            }

            if (step.Contains("Я авторизовался банковским пользователем"))
            {
                var loginRegex = "'.*'";

                var user = Regex.Match(step, loginRegex).Value.Replace("'", "").ToLower();

                if (user != CurrentBankUser)
                    RunNewBrowser(user);
            }
        }

        public static void RunNewBrowser(string ADLogin = null)
        {
            if (Browser != null)
                Browser.Quit();

            if (driverProcess != null)
                driverProcess.Kill();

            Browser = null;

            if (ADLogin != null)
                CurrentBankUser = ADLogin;
            else
                CurrentBankUser = "local";

            Initialize();
        }

        [AfterScenario]
        public void AfterScenario()
        {
            if(!ConfigurationManager.AppSettings["TestEnvironment"].Contains("Local") && Browser != null)
            {
                AllureReport.SetReport();

                var logger = NLog.LogManager.GetCurrentClassLogger();

                if(scenarioContext.TestError != null)
                {
                    Screenshots.MakeScreenshot();

                    logger.Trace($"{"\n" + ErrorHandling.Message(scenarioContext.TestError.ToString())}" + "\n");

                    logger.Trace($"{scenarioContext.TestError.ToString()}");
                }

                if(Convert.ToBoolean(ConfigurationManager.AppSettings["TestRail"]))
                    TestRailResults.TestRailResult();
            }
        }

        /// <summary>
        /// Форматирование токена для записи нового сертификата, запускает утилитку, доступную по PATH
        /// </summary>
        public static void FormatToken()
        {
            Process process = new Process();

            var startInfo = new ProcessStartInfo
            {
                WindowStyle = ProcessWindowStyle.Hidden,
                FileName = ProjectEnvironment.RtAdmin,
                Arguments = "-f -q",
            };

            process.StartInfo = startInfo;
            process.Start();

            process.WaitForExit();
        }
    }

    public class ActiveDirectory
    {
        public class User : IDisposable
        {
            public User(string userName, string domainName, string password)
            {
                BaseSteps.BrowserPort = (9000 + AppDomain.GetCurrentThreadId()).ToString();

                ProcessStartInfo processStartInfo = new ProcessStartInfo(ProjectEnvironment.ChromeDriverPath.Replace(@"\\", @"\"), $"--port={BaseSteps.BrowserPort}");
                processStartInfo.UserName = userName;
                System.Security.SecureString securePassword = new System.Security.SecureString();
                foreach (char c in password)
                {
                    securePassword.AppendChar(c);
                }
                processStartInfo.Password = securePassword;
                processStartInfo.Domain = domainName;
                processStartInfo.UseShellExecute = false;
                processStartInfo.LoadUserProfile = true;
                processStartInfo.WorkingDirectory = ProjectEnvironment.ChromeDriverDir;
                processStartInfo.WindowStyle = ProcessWindowStyle.Hidden;
                Thread startThread = new Thread(() =>
                                            {
                                                BaseSteps.driverProcess = Process.Start(processStartInfo);
                                                BaseSteps.driverProcess.WaitForExit();
                                            })
                                            { IsBackground = true };
                startThread.Start();
            }

            public void Dispose()
            {}
        }

        public static (string, string) GetPasswordAndDomainFromVault()
        {
            string pass, domain;

            var url = URLs.Vault + $"{BaseSteps.CurrentBankUser}";

            var webRequest = WebRequest.Create(url) as HttpWebRequest;

            webRequest.Headers.Add("X-Vault-Token", URLs.VaultToken);

            using (var s = webRequest.GetResponse().GetResponseStream())
            {
                using (var sr = new StreamReader(s))
                {
                    var response = (JObject)JsonConvert.DeserializeObject(sr.ReadToEnd());

                    pass = response.SelectToken("data.password").ToString();

                    var domainLogin = response.SelectToken("data.login").ToString();

                    domain = domainLogin.Substring(0, domainLogin.IndexOf(@"\"));
                }
            }

            return (pass, domain);
        }

        public static Process GetChromeDriverProcess()
        {
            var processes = new List<Process>();
                
            processes = Process.GetProcesses().ToList();

            var ADprocesses = new List<Process>();

            ADprocesses.AddRange(processes.FindAll(process => process.ProcessName.Equals("chromedriver")));

            var cmdlines = new List<string>();

            foreach (var process in ADprocesses)
            {
                try
                {
                    cmdlines.Add(GetCommandLine(process));
                }
                catch
                {
                    cmdlines.Add("С процессом что-то случилось.");
                }
            }

            return ADprocesses[cmdlines.FindIndex(cmd => cmd.Contains("--port=" + BaseSteps.BrowserPort))];
        }

        private static string GetCommandLine(Process process)
        {
                using (ManagementObjectSearcher searcher = new ManagementObjectSearcher("SELECT CommandLine FROM Win32_Process WHERE ProcessId = " + process.Id))
                using (ManagementObjectCollection objects = searcher.Get())
                {
                    return objects.Cast<ManagementBaseObject>().SingleOrDefault()?["CommandLine"]?.ToString();
                }
        }
    }
}
