using Allure.Commons;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using NUnit.Framework;
using OpenQA.Selenium;
using OpenQA.Selenium.Remote;
using Common;
using Sources.Common;
using PageUtils;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Xml;
using System.Threading;
using System;

namespace Utils
{
    public static class AllureReport
    {
        [ThreadStatic] private static List<LogEntry> logEntries = null;

        [ThreadStatic] private static List<string> RequestIDs = null;

        /// <summary>
        /// Перед выключением браузера кладем в переменную сообщения консоли, чтобы потом добавить в отчет аллюр
        /// </summary>
        public static void GetLogsFromBrowser()
        {
            logEntries = new List<LogEntry>();

            logEntries = BaseSteps.Browser.Manage().Logs.GetLog(LogType.Browser).ToList();
        }

        /// <summary>
        /// После выключения браузера кладем в переменную все реквест-айди из гигантского json-файла логов
        /// </summary>
        public static void GetRequestIDs()
        {
            List<JToken> headersWithRequest = new List<JToken>();

            var log = ProjectEnvironment.TestLogs + "\\" + BaseSteps.AllureScenarioUUID + ".json";

            while (log.IsFileLocked())
            {
                Thread.Sleep(500); // Если тест запущен под тестовой учеткой, то по какой-то причине может долго убиваться процесс драйвера, потому ждем, пока файл лога высвободится от занимающего его процесса
            }

            using (StreamReader sr = new StreamReader(log))
            {
                string file = sr.ReadToEnd();

                if (file.Substring(file.Length - 4).Contains(",")) // Json-файл логов может криво формироваться, что мешает его парсить. Проверям, что если в последних четырех символах файла содержится запятая
                {
                    file = file.TrimEnd('}') + "]}"; // тогда обрезаем стрингу файла до последней фигурной скобки и вставляем недостающие символы для нормального парсинга в json
                }

                var json = (JObject)JsonConvert.DeserializeObject(file); // десериализация в json

                try
                {

                    List<JToken> headers = json.SelectTokens("events[*].params.headers").ToList(); // вытаскиваем все headers запросов

                    headersWithRequest.AddRange(
                                                headers.FindAll(
                                                                header => header.ToString().ToLower().Contains("request-id")
                                                                &&
                                                                Regex.IsMatch(header.ToString().ToLower(), @"HTTP/1.1 [^2]\d{2}".ToLower())
                                                                )
                                                ); // ищем записи с request-id и статусом не 2ХХ
                }
                catch
                {
                    headersWithRequest = null;
                }
            }

            if (headersWithRequest != null)
            {
                RequestIDs = new List<string>();

                headersWithRequest.ForEach(header =>
                {
                    RequestIDs.Add(header.Children().First(child => child.ToString().ToLower().Contains("request-id")).ToString().ToLower().Replace("request-id: ", "")); // убираем объявление заголовка
                });

                RequestIDs = RequestIDs.Distinct().ToList(); // убираем дублирование (обязательно после удаления заголовков, иначе дубли останутся)
            }
        }

        public static void GetMonolithVersion()
        {
            string version;

            var webRequest = WebRequest.Create(URLs.Bank + "api/health") as HttpWebRequest;

            webRequest.ContentType = "application/json";
            webRequest.UserAgent = "Nothing";

            using (var s = webRequest.GetResponse().GetResponseStream())
            {
                using (var sr = new StreamReader(s))
                {
                    var health = (JObject)JsonConvert.DeserializeObject(sr.ReadToEnd());

                    version = health["version"].Value<string>();
                }
            }

            BaseSteps.DBOMonolithVersion = version;
        }

        static bool IsFileLocked(this string path)
        {
            try
            {
                using (Stream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.None))
                {
                    stream.Close();
                }
            }
            catch (IOException)
            {
                return true;
            }

            return false;
        }

        public static void AddConsoleLogAndRequestIDs()
        {
            string ConsoleLogAndRequestIDs = $"Все request-id со статусом, отличным от 2ХХ (Всего таких request-id - {RequestIDs.Count}):\n";

            for (int i = 0; i < RequestIDs.Count; i++)
            {
                ConsoleLogAndRequestIDs += $"\n{i + 1}:\n";

                for (int j = 0; j < URLs.SEQ.Count; j++)
                {
                    ConsoleLogAndRequestIDs += $"{URLs.SEQ[j]}?filter=RequestId%20%3D%20'{RequestIDs[i]}'\n";
                }
            }

            ConsoleLogAndRequestIDs += "\n\nСсылки на SEC:";

            URLs.SEQ.ForEach(url => ConsoleLogAndRequestIDs += $"\n{url}");

            ConsoleLogAndRequestIDs += "\n\n\nОшибки работы в браузере со вкладки \"Console\":\n\n";

            logEntries.ForEach(entry => ConsoleLogAndRequestIDs += entry.Timestamp + "\n" + entry.Message + "\n\n");

            string logPath = ProjectEnvironment.AllureResults + @"\" + BaseSteps.AllureScenarioUUID + ".log";

            File.WriteAllText(logPath, ConsoleLogAndRequestIDs);

            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x => x.attachments.Add(new Attachment
            {
                name = "Лог консоли браузера и request-id's всех запросов с отличным от 2ХХ кодом ответа.",
                source = BaseSteps.AllureScenarioUUID + ".log",
                type= "text/plain"
            }));
        }

        public static void SetReport()
        {
            if (BaseSteps.Browser != null)
            {
                GetMonolithVersion();   // Дергаем healthcheck стенда для получения версии бэкенда
                SetAppVersion();        // Регистрируем версию приложения в тест-кейсе
                SetAllureEnvironment(); // Заполняем данные об текущем окружении в тест-кейс

                if (BaseSteps.scenarioContext.TestError != null)
                    UpdateStatusMessage();  // Если тест упал, проверяем контроли на странице и их текст пишем в красный блок ошибки теста
            }

            SetJiraLinks();         // Если есть ссылка на баг в жире в тегах, то делаем тест желтым и добавляем ссылку в аллюре
            SetHostName();          // Проверяем хост-нейм и меняем на норм название на вкладке TimeLine
            UpdateStory();          // Добавляем описание из фичи для группировки тестов в отчете в секции Behaviors
            SetSuite();             // Раскидываем тесты в отчете по наборам тестов во вкладке Suites
            SetSeverity();          // Выбираем уровень теста из тегов
            SetPackage();           // Выбираем в чей регресс можно определить этот тест по тегам

        }

        public static void SetAppVersion()
        {
            // Один раз пишем версию приложения вверху тест-кейса в отчете аллюр
            if(BaseSteps.DBOUIversion.Contains("1."))
            {
                AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
                {
                    x.descriptionHtml = $"<table border=\"2\">" +
                    $"<tr><td width=\"150\">Версия <a href=\"https://git-01.psbnk.msk.ru/ivanovrs/rbp-ui/pipelines\">UI</a></td><td width=\"150\" style=\"text-align:center\">{BaseSteps.DBOUIversion}</td></tr>" +
                    $"<tr><td width=\"150\">Версия <a href=\"https://git-01.psbnk.msk.ru/ivanovrs/rbp-mth/pipelines\">Monolith</a></td><td width=\"150\" style=\"text-align:center\">{BaseSteps.DBOMonolithVersion}</td></tr>" +
                    $"</table>";
                });
            }
        }

        public static void SetAllureEnvironment()
        {
            Directory.CreateDirectory(ProjectEnvironment.AllureResults);

            if(TestContext.CurrentContext.Test.Properties.Get("Description").ToString().Contains("Успешная авторизация клиента"))
            {
                File.Delete(ProjectEnvironment.AllureResults + "/environment.xml");
                File.Delete(ProjectEnvironment.AllureResults + "/categories.json");

                XmlDocument env = new XmlDocument();

                env.Load(ProjectEnvironment.Allure + "/environment.xml");

                XmlElement root = env.DocumentElement;

                ICapabilities capabilities = ((RemoteWebDriver)BaseSteps.Browser).Capabilities;

                var parameter = root.SelectSingleNode("//key[text() = 'Browser, version'] / ancestor::parameter / value").FirstChild;

                parameter.InnerText = capabilities.GetCapability("browserName").ToString() + 
                            ", " + capabilities.GetCapability("browserVersion").ToString();

                if (BaseSteps.DBOUIversion.Contains("1."))
                {
                    parameter = root.SelectSingleNode("//key[text() = 'UI version'] / ancestor::parameter / value");
                    parameter.InnerText = BaseSteps.DBOUIversion;
                }

                parameter = root.SelectSingleNode("//key[text() = 'Monolith version'] / ancestor::parameter / value");
                parameter.InnerText = BaseSteps.DBOMonolithVersion;

                parameter = root.SelectSingleNode("//key[text() = 'Bank URL'] / ancestor::parameter / value");
                parameter.InnerText = URLs.Bank;

                parameter = root.SelectSingleNode("//key[text() = 'Client URL'] / ancestor::parameter / value");
                parameter.InnerText = URLs.Client;
                
                parameter = root.SelectSingleNode("//key[text() = 'Runner platform'] / ancestor::parameter / value");
                parameter.InnerText = capabilities.GetCapability("platformName").ToString();

                env.Save(ProjectEnvironment.AllureResults + "/environment.xml");

                File.Copy(ProjectEnvironment.Allure + "/categories.json", ProjectEnvironment.AllureResults + "/categories.json");
            }
        }

        public static void SetTestRailStatus(string message)
        {
            if (TestContext.CurrentContext.Test.Properties.Get("Description").ToString().Contains("Успешная авторизация клиента"))
            {
                XmlDocument env = new XmlDocument();

                env.Load(ProjectEnvironment.AllureResults + "/environment.xml");

                XmlElement root = env.DocumentElement;

                var parameter = root.SelectSingleNode("//key[text() = 'TestRail'] / ancestor::parameter / value").FirstChild;
                parameter.InnerText = message;

                env.Save(ProjectEnvironment.AllureResults + "/environment.xml");
            }
        }

        public static void SetJiraLinks()
        {
            List<string> jiraLinks = new List<string>();

            jiraLinks.AddRange(BaseSteps.scenarioTags.FindAll(x => x.Contains("PSB")));

            if(jiraLinks.Count != 0)
            {
                AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x => x.status = Status.none);

                jiraLinks.ForEach(link =>
                    AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
                    {
                        if(!x.links.Exists(t => t.url.Contains(link.ToString())))
                            x.links.Add(Link.Issue("Ссылка на ошибку в Jira", @"https://jira.psbnk.msk.ru/browse/" + $"{link.ToString()}"));

                        if (BaseSteps.scenarioContext.TestError == null)
                            x.statusDetails.message = "Успешно прошедшие тесты (нужно закрыть задачу и удалить тег)";
                    }));
            }
        }

        public static void SetHostName()
        {
            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
            {
                int i = -1;

                i = x.labels.FindIndex(t => t.name == "host");

                try
                {
                    x.labels[i].value = names[x.labels[i].value];
                }
                catch { };
            });
        }

        public static void UpdateStory()
        {
            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
            {
                if (x.labels.FindIndex(t => t.name.Equals("story")) == -1)
                    x.labels.Add(Label.Story(BaseSteps.featureContext.FeatureInfo.Description));
            });
        }

        public static void SetSuite()
        {
            string className = TestContext.CurrentContext.Test.ClassName;

            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
            {
                if (x.labels.FindIndex(t => t.name.Equals("parentSuite")) == -1)
                    x.labels.Add(Label.ParentSuite(parentSuites[parentSuites.Keys.First(key => className.Contains(key))]));
            });

            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
            {
                if (x.labels.FindIndex(t => t.name.Equals("suite")) == -1)
                    x.labels.Add(Label.Suite(suites[suites.Keys.First(key => className.Contains(key))]));
            });
        }

        public static void SetSeverity()
        {
            var severityTag = BaseSteps.scenarioTags.Intersect(severity.Keys).FirstOrDefault();

            if (severityTag != null)
            {
                AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
                {
                    if (x.labels.FindIndex(label => label.name.Equals("severity")) == -1)
                        x.labels.Add(Label.Severity(severity[severityTag]));
                });
            }
        }

        public static void SetPackage()
        {
            var package = BaseSteps.scenarioTags.Intersect(regress.Keys).FirstOrDefault();

            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
            {
                if (x.labels.FindIndex(label => label.name.Equals("package")) == -1)
                {
                    if (package != null)
                        x.labels.Add(Label.Package(regress[package]));
                    else
                        x.labels.Add(Label.Package("Общие регрессионные тесты"));
                }
            });
        }

        public static string GetControlsFromPage()
        {
            var alertMessageElements = BaseSteps.Browser.FindElements(Locators.AlertMessage).ToList();

            alertMessageElements.AddRange(BaseSteps.Browser.FindElements(By.XPath("//*[@ng-if = 'alert.message']")));

            if (alertMessageElements.Count != 0)
            {
                string alertMessages = "\nКонтроли на странице в момент падения теста: \n";

                alertMessageElements.ForEach(elem => alertMessages = alertMessages + "\"" + elem.Text + "\"\n");

                return alertMessages;
            }
            else
            {
                return null;
            }

        }

        public static void UpdateStatusMessage()
        {
            string controls = GetControlsFromPage();

            AllureLifecycle.Instance.UpdateTestCase(BaseSteps.AllureScenarioUUID, x =>
            {
                if (controls != null)
                {
                    x.statusDetails.trace = x.statusDetails.message + controls + "\n\n" + x.statusDetails.trace;
                }
                else
                    x.statusDetails.trace = x.statusDetails.message + "\nКонтролей на странице в момент падения теста не было." + "\n\n" + x.statusDetails.trace;
            });
        }

        public static Dictionary<string, string> parentSuites = new Dictionary<string, string>
        {
            { "Tests.Client",       "Клиент" },
            { "Tests.Bank",         "Банк" },
            { "Tests.Interaction",  "Клиент-Банк" },
            { "Tests.Interface",    "Интерфейс" }
        };

        public static Dictionary<string, string> suites = new Dictionary<string, string>
        {
            { "Client.Accounts",            "Счета и выписки" },
            { "Client.Corporations",        "Корпорации. СУФК" },
            { "Client.CurrencyOperations",  "Валютные операции" },
            { "Client.DepositsDeals",       "Депозитные сделки" },
            { "Client.E_Doc",               "E-Doc" },
            { "Client.Loans",               "Кредиты" },
            { "Client.RublePayments",       "Рублевые платежные поручения" },
            { "Client.UserAccess",          "Доступ и полномочия" },
            
            { "Bank.UserAccess",                    "Доступ" },
            { "Bank.RoleModel",                     "Ролевая модель" },
            { "Bank.Documents.E_Doc",               "E-Doc" },
            { "Bank.Documents.Loans",               "Кредиты" },
            { "Bank.Documents.Payments",            "Рублевые документы" },
            { "Bank.Documents.Guarantees",          "Банковские гарантии" },
            { "Bank.Documents.CurrencyOperations",  "Валютные документы" },
       
            { "Interaction.Letters",        "Переписка" },
            { "Interface.Паджинация",       "Паджинация" },            
        };

        public static Dictionary<string, SeverityLevel> severity = new Dictionary<string, SeverityLevel>
        {
            { "Средний",        SeverityLevel.normal },
            { "Блокирующий",    SeverityLevel.blocker },
            { "Критический",    SeverityLevel.critical },
            { "Низкий",         SeverityLevel.minor },
            { "Тривиальный",    SeverityLevel.trivial }
        };
    }
}
