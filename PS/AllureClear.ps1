param($AllureEnvironment)

if($AllureEnvironment -eq 'StagingAllure')
{
    $path               = 'D:\Tests_UI\E2E-Tests'

    $WebSiteFilePath    =  "\\dbotst-app01\c$\inetpub\wwwroot\allure\E2E-Tests"
}

elseif($AllureEnvironment -eq 'DevAllure')
{
    $path               = 'D:\Tests_UI\E2E-dev'

    $WebSiteFilePath    =  "\\dbotatst-app01\c$\inetpub\wwwroot\allure\E2E-dev"
}

elseif($AllureEnvironment -eq 'IEAllure')
{
    $path               = 'D:\Tests_UI\E2E-IE'

    $WebSiteFilePath    =  "\\dbotst-app01\c$\inetpub\wwwroot\allure\E2E-IE"
}

elseif($AllureEnvironment -eq 'YandexAllure')
{
    $path               = 'D:\Tests_UI\E2E-Ya'

    $WebSiteFilePath    =  "\\dbotst-app01\c$\inetpub\wwwroot\allure\E2E-Ya"
}

Function ClearBuildHistory
    {
        Write-Host ===============
        Write-Host ****
        Write-Host 'Local History Removing! We Remove that Folders:'
        Write-Host ****
        Get-ChildItem -Path $path -Recurse | Where-Object {$_.Attributes -match 'Directory'} | Select-Object Name,Attributes
		Get-ChildItem -Path $path -Recurse | Where-Object {$_.Attributes -match 'Directory'} | remove-item  -Recurse -force
		Write-Host ****
        Write-Host 'Removing is done!'
        Write-Host ****
    }

Function ClearWebSiteHistory 
    {
	    Write-Host ===============
        Write-Host ****
        Write-Host 'Web Site History Removing! We Remove that Folders:'
        Write-Host ****
        Get-ChildItem -Path $WebSiteFilePath -Recurse | Where-Object {$_.Attributes -match 'Directory'} | Select-Object Name,Attributes
		Get-ChildItem -Path $WebSiteFilePath -Recurse | Where-Object {$_.Attributes -match 'Directory'} | remove-item  -Recurse -force
		Write-Host ****
        Write-Host 'Removing is done!'
        Write-Host ****
    }

Write-Host $AllureEnvironment
ClearBuildHistory
ClearWebSiteHistory