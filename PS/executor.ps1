param($PipelineID)

Write-Host ===============
Write-Host ****
Write-Host 'Change executor.json'

$json = Get-Content './Allure/executor.json' -raw | ConvertFrom-Json
$json.buildOrder    = "$PipelineID"
$json.buildName     = "Пайплайн тестов $PipelineID"
$json.buildUrl      = "https://git-01.psbnk.msk.ru/remote-banking-platform/rbp-uitests/pipelines/$PipelineID"
$json | ConvertTo-Json | set-content './allure-results/executor.json'

Write-Host ****
Write-Host ===============
