param($option)

$s = Get-WmiObject Win32_Process | Where-Object { $_.commandline -match 'signer' } | Select-Object processid

if($option -ieq 'on')
{
    Write-Host 'Check: Signer is on?'

    $s.Count

    if ($s.Count -eq 0)
    {
        Set-Location ./Rbp-Signer
        
        Start-Process -windowstyle hidden cmd -Argument '/c dotnet run -p Rbp.Signer 9999'
        
        Write-Host 'Signer enabled'
    }
    else {
        Write-Host 'Signer already running'
    }
}
elseif ($option -ieq 'off') {
    
    Write-Host 'Stopping Signer for unlock next jobs'

    Stop-Process -Id $s.processid

    Write-Host 'Signer stopped'
}
