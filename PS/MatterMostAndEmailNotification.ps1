param ($Environment, $Driver, $SendMatterMost, $SendEmail)

$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12' 
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols 

$Logs = Get-ChildItem .\NunitLogs\*.xml

[int]$TotalTests = 0
[int]$FailedTests = 0
[int]$FailedGoz = 0

foreach ($log in $Logs)
{
    [xml]$LogFile = Get-Content $log
    
    $TestRunNode = $LogFile.SelectSingleNode('/test-run')

    $TotalTests = $TotalTests + [System.Convert]::ToInt32($TestRunNode.total, 10)
    $FailedTests = $FailedTests + [System.Convert]::ToInt32($TestRunNode.failed, 10)
}

$GOZ = '.\NunitLogs\GOZ.xml'
if (Test-Path -Path $GOZ)
{
    $GOZlog = Get-ChildItem $GOZ

    [xml]$GOZ = Get-Content $GOZlog
    
    $gozfailed = $GOZ.SelectSingleNode('/test-run')

    $FailedGoz = $gozfailed.failed
}

$activeTestRunners = 0

$runnerNames = 0

$runners = Invoke-RestMethod -Method GET -Uri https://git-01.psbnk.msk.ru/api/v4/projects/748/runners -Headers @{'PRIVATE-TOKEN' = "$env:api" }

foreach ($runner in $runners)
    {
        $id = $runner.id 
        
        $runnerInfo = Invoke-RestMethod -Method GET -Uri https://git-01.psbnk.msk.ru/api/v4/runners/$id -Headers @{'PRIVATE-TOKEN' = "$env:api" }
        
        if($runnerInfo.online -eq 'True' -and $runnerInfo.tag_list -match 'test')
            {
                $activeTestRunners = $activeTestRunners + 1
                
                if ($runnerNames -eq 0)
                    {
                        $runnerNames = $runnerInfo.description
                    }
                else
                    {
                        $runnerNames = $runnerNames + ', ' + $runnerInfo.description
                    }
            }
    }

if($Environment -eq 'staging' -and $Driver -eq 'Chrome')
{
    $TestDirectory          = "D:\Tests_UI\E2E-Tests"
}

elseif($Environment -eq 'master')
{
      $TestDirectory          = "D:\Tests_UI\E2E-dev"
}

elseif($Driver -eq 'Yandex')
{
      $TestDirectory          = "D:\Tests_UI\E2E-Ya"
}

elseif($Driver -eq 'IE')
{
      $TestDirectory          = "D:\Tests_UI\E2E-IE"
}

Set-Location $TestDirectory

$json = Get-Content './allure-report/widgets/summary.json' -raw | ConvertFrom-Json

$hours = (New-Timespan -Seconds ($json.time.duration/1000)).Hours
$minutes = (New-Timespan -Seconds ($json.time.duration/1000)).Minutes
$seconds = (New-Timespan -Seconds ($json.time.duration/1000)).Seconds

$testsWithBug = $json.statistic.unknown

$Allure
$EnvironmentAndDriver
$text

if ($Environment -eq 'staging' -and $Driver -eq 'Chrome')
{
    $Allure = 'https://rbp-bankstaging/allure/'
    
    $EnvironmentAndDriver = 'Staging, Chrome'
    
    [string]$text = "|Environment|$EnvironmentAndDriver|\n|:-|:-:|\n| Total number of tests |:white_check_mark: $TotalTests |\n| Failed tests | :exclamation: $FailedTests |\n    | Failed GOZ | :bangbang: $FailedGoz |\n | Broken tests (with link to bug) | :bug: $testsWithBug  |\n| Allure | $Allure |\n  | Test-stage duration | $hours h. $minutes m. $seconds s.|\n  | Active test runners | $activeTestRunners ($runnerNames) |\n  | DBO-links | [Bank](https://tstdbokb-bank/),[Client](https://tstdbokb.psbnk.msk.ru/)|"
}
elseif ($Driver -eq 'Yandex')
{
    $Allure = 'https://rbp-bankstaging/allure-Yandex/'
    
    $EnvironmentAndDriver = 'Staging, Yandex'

    [string]$text = "|Environment|$EnvironmentAndDriver|\n    |:-|:-:|\n    | Total number of tests |:white_check_mark: $TotalTests |\n    | Failed tests | :exclamation: $FailedTests |\n    | Failed GOZ | :bangbang: $FailedGoz |\n    | Allure | $Allure |\n    | DBO-links | [Bank](https://tstdbokb-bank/),[Client](https://tstdbokb.psbnk.msk.ru/)|"
}
elseif ($Driver -eq 'IE')
{
    $Allure = 'https://rbp-bankstaging/allure-ie/'
    
    $EnvironmentAndDriver = 'Staging, Internet Explorer'
    
    [string]$text = "|Environment|$EnvironmentAndDriver|\n    |:-|:-:|\n    | Total number of tests |:white_check_mark: $TotalTests |\n    | Failed tests | :exclamation: $FailedTests|\n    | Allure | $Allure |\n    | DBO-links | [Bank](https://tstdbokb-bank/),[Client](https://tstdbokb.psbnk.msk.ru/)|"
}
elseif ($Environment -eq 'master')
{
    $Allure = 'https://rbp-clientapp/allure/'
    
    $EnvironmentAndDriver = 'Master, Chrome'
    
    [string]$text = "| Environment |$EnvironmentAndDriver|\n|:-|:-:|\n| Total number of tests |:white_check_mark: $TotalTests |\n| Failed tests | :exclamation: $FailedTests |\n| Broken tests (with link to bug) | :bug: $testsWithBug  |\n| Allure | $Allure  |\n | Test-stage duration | $hours h. $minutes m. $seconds s.|\n  | Active test runners | $activeTestRunners ($runnerNames) |\n  | DBO-links | [Bank](https://rbp-app.headoffice.psbank.local/), [Client](https://rbp-clientapp/)|"
}

if ($SendMatterMost -eq 'true')
{
    $payload = "payload={""username"": ""Specflow"", ""channel"": ""specflow"", ""text"": ""$text""}"
    
    Write-Host $payload
    
    Invoke-WebRequest -Uri https://mm-01/hooks/n7aup4sz97ngdr7bquqppgpdey -Body $payload -Method POST
}

if ($SendEmail -eq 'true')
{
    $To =  @("madvlaydin@gmail.com")

    $encoding = [System.Text.Encoding]::UTF8
    $From = "SpecFlowAutoTests@psbank.ru"
    $Subject = "Предрелизный прогон автотестов на стейджинге"
    $Body = @"
    Environment             → $EnvironmentAndDriver
    Общее количество тестов → $TotalTests
    Упавшие тесты           → $FailedTests
    Упавшие тесты ГОЗ       → $FailedGoz
    Allure                  → $Allure
"@

    $SMTPServer = "relay.psbank.ru"
    $SMTPPort = "25"
    Send-MailMessage -From $From -to $To -Subject $Subject -Body $Body -SmtpServer $SMTPServer -port $SMTPPort -Encoding $encoding
}
