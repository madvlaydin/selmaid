param($PipelineID, $Environment, $Driver)

if($Environment -eq 'staging' -and $Driver -eq 'Chrome')
{
      $AllureResults          = "D:\Tests_UI\E2E-Tests\allure-results"
      $TestDirectory          = "D:\Tests_UI\E2E-Tests"
      $ReportName             = "PSB Corporate End-to-End tests"
      $ComputerName           = "DBOTST-APP01"
      $PoolName               = "allure"
      $HostingDirectory       = "\\dbotst-app01\c$\inetpub\wwwroot\allure\"
      $WebConfigDirectory     = "\\dbotst-app01\c$\inetpub\wwwroot\allure\E2E-Tests\allure-report"
}

elseif($Environment -eq 'master')
{
      $AllureResults          = "D:\Tests_UI\E2E-dev\allure-results"
      $TestDirectory          = "D:\Tests_UI\E2E-dev"
      $ReportName             = "Developer stand tests. PSB Corp"
      $ComputerName           = "DBOTATST-APP01"
      $PoolName               = "allure"
      $HostingDirectory       = "\\dbotatst-app01\c$\inetpub\wwwroot\allure\"
      $WebConfigDirectory     = "\\dbotatst-app01\c$\inetpub\wwwroot\allure\E2E-dev\allure-report"
}

elseif($Driver -eq 'Yandex')
{
      $AllureResults          = "D:\Tests_UI\E2E-Ya\allure-results"
      $TestDirectory          = "D:\Tests_UI\E2E-Ya"
      $ReportName             = "Yandex Browser E2E-tests"
      $ComputerName           = "DBOTST-APP01"
      $PoolName               = "allure-Yandex"
      $HostingDirectory       = "\\dbotst-app01\c$\inetpub\wwwroot\allure\"
      $WebConfigDirectory     = "\\dbotst-app01\c$\inetpub\wwwroot\allure\E2E-Ya\allure-report"
}

elseif($Driver -eq 'IE')
{
      $AllureResults          = "D:\Tests_UI\E2E-IE\allure-results"
      $TestDirectory          = "D:\Tests_UI\E2E-IE"
      $ReportName             = "INTERNET EXPLORER SLOWLY TESTS"
      $ComputerName           = "DBOTST-APP01"
      $PoolName               = "allure-ie"
      $HostingDirectory       = "\\dbotst-app01\c$\inetpub\wwwroot\allure\"
      $WebConfigDirectory     = "\\dbotst-app01\c$\inetpub\wwwroot\allure\E2E-IE\allure-report"
}

Write-Host ===============
Write-Host ****
Write-Host "Create directory for allure test results if not exists: " $AllureResults

If(!(test-path $AllureResults))
{
      New-Item -ItemType Directory -Force -Path $AllureResults -Verbose

      Write-Host "Directory created!"
}
else{
      Remove-Item -path $AllureResults/environment.xml -Force -Recurse -ErrorAction Continue -Verbose
      Write-Host "environment.xml deleted!"
      
      Remove-Item -path $AllureResults/categories.json -Force -Recurse -ErrorAction Continue -Verbose
      Write-Host "categories.json deleted!"
      
      Remove-Item -path $AllureResults/executor.json -Force -Recurse -ErrorAction Continue -Verbose
      Write-Host "executor.json deleted!"
}

Write-Host ****
Write-Host ===============

Write-Host ===============
Write-Host ****
Write-Host 'Copy allure-results'
Copy-Item -Path ./allure-results/* -Destination $TestDirectory\allure-results -ErrorAction Continue
Write-Host ****
Write-Host ===============

Set-Location $TestDirectory -Verbose

Write-Host ===============
Write-Host ****
Write-Host 'Save actual history'
Copy-Item -Path ./allure-report/history -Recurse -Destination "./$PipelineID-history" -ErrorAction Continue -Verbose
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Delete old history'
Remove-Item -path ./allure-results/history -Force -Recurse -ErrorAction Continue -Verbose
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Copy actual history'
New-Item -ItemType Directory -Force -Path ./allure-results/history
Copy-Item "./$PipelineID-history/*" -Recurse -Destination ./allure-results/history -ErrorAction Continue -Verbose
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Allure generate'
allure generate --clean
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Change title of report'

$json = Get-Content './allure-report/widgets/summary.json' -raw | ConvertFrom-Json
$json.reportName = $ReportName
$json | ConvertTo-Json | set-content './allure-report/widgets/summary.json'

Write-Host ****
Write-Host ===============

Write-Host ===============
Write-Host ****
Write-Host 'Stop pool'
Invoke-Command -ComputerName $ComputerName -UseSSL -ScriptBlock { Stop-WebAppPool -Name $PoolName }
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Copy allure-results and allure-report from build to staging'
copy-item $TestDirectory -destination $HostingDirectory -recurse -Force
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Copy web.config'
copy-item "D:\Tests_UI\E2E-Tests\web.config" -destination $WebConfigDirectory -Force -Verbose
Write-Host ****

Write-Host ===============
Write-Host ****
Write-Host 'Start pool'
Invoke-Command -ComputerName $ComputerName -UseSSL -ScriptBlock { Start-WebAppPool -Name $PoolName }
Write-Host ****
Write-Host ===============
