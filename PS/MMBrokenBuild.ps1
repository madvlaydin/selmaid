$AllProtocols = [System.Net.SecurityProtocolType]'Ssl3,Tls,Tls11,Tls12' 
[System.Net.ServicePointManager]::SecurityProtocol = $AllProtocols

$jobs = Invoke-RestMethod -Method GET -Uri https://git-01.psbnk.msk.ru/api/v4/projects/748/pipelines/$env:CI_PIPELINE_ID/jobs -Headers @{'PRIVATE-TOKEN' = "$env:api" }

$ids = $jobs | Where status -eq "failed" | Select id

$id = $ids[0].id

[string]$text = "$env:GITLAB_USER_LOGIN, you broke the build, see on https://git-01/remote-banking-platform/rbp-uitests/-/merge_requests/$env:CI_MERGE_REQUEST_IID \nLog of failed build job here: https://git-01.psbnk.msk.ru/remote-banking-platform/rbp-uitests/-/jobs/$id"

$payload = "payload={""username"": ""Specflow"", ""channel"": ""specflow"", ""text"": ""$text""}"
    
Write-Host $payload
    
Invoke-WebRequest -Uri https://mm-01/hooks/n7aup4sz97ngdr7bquqppgpdey -Body $payload -Method POST
