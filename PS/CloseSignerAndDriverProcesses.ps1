Write-Host 'Stopping ChromeDrivers and Browsers processes for unlock next pipelines'

$s = Get-WmiObject Win32_Process | Where-Object { $_.commandline -match 'remote-banking-platform\\rbp-uitests\\Rbp.UITests.SpecFlow\\resources\\chrome\\chromedriver.exe' } | Select-Object processid

Stop-Process -Id $s.processid

Write-Host 'ChromeDrivers stopped'

$s = Get-WmiObject win32_process | Where-Object {$_.commandline -match 'Chromium'} | Select-Object processid

Stop-Process -Id $s.processid

Write-Host 'Chrome stopped'

Stop-Process -name 'browser'

Write-Host 'Yandex Browsers stopped'

Write-Host 'Stopping Signer for unlock next pipelines'

$s = Get-WmiObject Win32_Process | Where-Object { $_.commandline -match 'signer' } | Select-Object processid

Stop-Process -Id $s.processid

Write-Host 'Signer stopped'
