param($Attribute, $Value)

$config = '.\Rbp.UITests.SpecFlow\App.config'

Write-Host 'Load App.config'

$xml = [xml](Get-Content -Path $config)

Write-Host "Set key $Attribute value to $Value"

$xml.SelectSingleNode("configuration/appSettings/add[@key='$Attribute']").SetAttribute("value", "$Value")

$xml.Save($config)