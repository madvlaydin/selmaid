#### Проект для хранения всяких полезных штучек

<details>
  <summary>Всякое</summary>
  

###### Метод проверки наличия строки в атрибутах веб-элемента:
```
        public static bool GetOuterHTMLValues(IWebDriver Browser, By elementSelector, string searchSubString)
        {
            IWebElement element = Browser.FindElement(elementSelector);

            string HTMLValue = ((IJavaScriptExecutor)Browser).ExecuteScript("return arguments[0].outerHTML;", element).ToString();

            return HTMLValue.Contains(searchSubString);
        }

```

###### Отправка файла в поле инпута:
```
        public static void UploadFile(IWebDriver Browser, string filename, string filetype)
        {
            string fileName = Path.Combine(Path.GetFullPath(Path.GetTempPath()), $"{filename}" + "." + $"{filetype}");

            File.WriteAllBytes(fileName, Convert.FromBase64String("MTIz"));

            Browser.FindElement(By.XPath("//input[@type='file']")).SendKeys(fileName);
        }
```

###### Скрипт получения полного xpath для переданного на вход элемента:
```
        public static string GetAbsoluteXPath(IWebDriver Browser, IWebElement element)
        {
            return (string)((IJavaScriptExecutor)Browser).ExecuteScript(
                 "function absoluteXPath(element) {" +
                   "var comp, comps = [];" +
                     "var parent = null;" +
                     "var xpath = '';" +
                     "var getPos = function(element) {" +
                     "var position = 1, curNode;" +
                     "if (element.nodeType == Node.ATTRIBUTE_NODE) {" +
                     "return null;" +
                     "}" +
                     "for (curNode = element.previousSibling; curNode; curNode = curNode.previousSibling) {" +
                         "if (curNode.nodeName == element.nodeName) {" +
                             "++position;" +
                         "}" +
                     "}" +
                     "return position;" +
                     "};" +

                     "if (element instanceof Document) {" +
                         "return '/';" +
                     "}" +

                     "for (; element && !(element instanceof Document); element = element.nodeType == Node.ATTRIBUTE_NODE ? element.ownerElement : element.parentNode) {" +
                     "comp = comps[comps.length] = {};" +
                     "switch (element.nodeType) {" +
                     "case Node.TEXT_NODE:" +
                         "comp.name = 'text()';" +
                         "break;" +
                     "case Node.ATTRIBUTE_NODE:" +
                         "comp.name = '@' + element.nodeName;" +
                         "break;" +
                     "case Node.PROCESSING_INSTRUCTION_NODE:" +
                         "comp.name = 'processing-instruction()';" +
                         "break;" +
                     "case Node.COMMENT_NODE:" +
                         "comp.name = 'comment()';" +
                         "break;" +
                     "case Node.ELEMENT_NODE:" +
                         "comp.name = element.nodeName;" +
                         "break;" +
                     "}" +
                     "comp.position = getPos(element);" +
                     "}" +

                     "for (var i = comps.length - 1; i >= 0; i--) {" +
                         "comp = comps[i];" +
                         "xpath += '/' + comp.name.toLowerCase();" +
                         "if (comp.position !== null) {" +
                         "xpath += '[' + comp.position + ']';" +
                         "}" +
                     "}" +
                     "return xpath;" +

                 "} return absoluteXPath(arguments[0]);", element);
        }

```

###### GitFlow
```mermaid
graph BT

    subgraph Master
    initial{fa:fa-gitlab}-->|git commit master1|master1(master1)
    master1(master1)-->|git merge feature into master|merge1((Merge commit))
    end
    
    subgraph feature
    sbindex(index)-->|git commit -m begin|sb1
    sb1(begin)-->|git commit -m work|sb2
    sb2(work)-->|git commit -m end work|sb3
    end

    master1(master1)-->|git checkout -b feature|sbindex
    sb3(end work)-->|merge-request|merge1((Merge commit))

    style initial fill:#f00,stroke:#000,stroke-width:3px
    style initial fill:#f00,stroke:#000,stroke-width:3px
    style master1 fill:#f00,stroke:#000,stroke-width:3px
    style merge1 fill:#f00,stroke:#000,stroke-width:3px
    style sb1 fill:#0ff,stroke:#000,stroke-width:3px
    style sb2 fill:#0ff,stroke:#000,stroke-width:3px
    style sb3 fill:#0ff,stroke:#000,stroke-width:3px
    style sbindex fill:#fff,stroke:#000,stroke-width:3px,stroke-dasharray: 10, 10
```

###### Gitlab CI
```mermaid
sequenceDiagram
    participant Developers
    participant QA
    participant Gitlab
    participant Runner
    participant Allure
    loop Target autotesting process
    Note over Gitlab: Test sources
        Gitlab->>QA: git pull
        loop feature
            Note over QA: Local work
            QA->>QA: commit
        end
        QA->>Gitlab: git push
        Gitlab->Gitlab: git merge into master
        Gitlab->>Runner: Pipeline
        loop Autotests
            Runner->Runner: Build
            Runner->>Runner: Testing
            Runner->Runner: Allure-results
            Runner->>Gitlab: Upload artifacts
        end
    Note over Gitlab: Artifacts on gitlab
        Gitlab->>Allure: Download artifacts
            Allure->>Allure: copy test history
            Allure->Allure: allure generate --clean
        loop Continuous delivery of test results to consumers
            Note over Allure: Current testing report
            Allure->>QA: 
            Allure->>Developers: 
        end
    end
```

  
</details>
